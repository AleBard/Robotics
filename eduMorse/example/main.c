#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <time.h>

#include "morse.h"
#include "socket.h"


/* Structure used to store the data of the circonferences used for trilateration.*/
struct circonference {
	double x,y,r;
};



/* This method returns the values related to the actual position of the robot and the distance from the goal from the sensors. */
void getSensorsMeasurements(FILE *f,char *parent,Pose* pose,proxMeas* proxmeas) {
	 // Get the pose measurement
	int flag=0;
    flag = getPose(f, parent, "pose", pose);
    //printf("posX %f , posY %f\n",pose->x,pose->y);
    if ( flag < 0 ) {
        printf("Errore 3\n");
        fclose(f);
        return;
    }

	// Get the proximity measurement
	flag = getProx(f,parent,"prox",proxmeas);
	//printf("dist %f\n",proxmeas->objects[0].dist);
	if(flag<0){
		printf("Errore 5 \n");
		fclose(f);
		return;
	}
	
	
	
}



/* Once the three reference circles have been stored, this method computes the coordinates of the goal, using the
 trilateration. The offset parameters are used to normalize the coordinates with respect to the position of the robot.*/
void trilateration(struct circonference *circ,double offsetX,double offsetY,double *goal){
    
    // First step: Translation of the axis.
    double temp1,temp2;
    double circ1[3]={0,0,circ[0].r};
    double circ2[3]={circ[1].x+offsetX,circ[1].y+offsetY,circ[1].r};
    double circ3[3]={circ[2].x+offsetX,circ[2].y+offsetY,circ[2].r};
    
    // Second step: rotation of the axis.
    double theta = acos(circ2[0]/sqrt((circ2[0]*circ2[0])+(circ2[1]*circ2[1])));
    
    printf("L' angolo di rotazione è di %f radianti\n",theta);
    temp1=circ2[0];
    temp2=circ2[1];
    circ2[0]=temp1*cos(-theta)-temp2*sin(-theta);
    circ2[1]=temp1*sin(-theta)+temp2*cos(-theta);
    circ2[2]=circ[1].r;
    
    temp1=circ3[0];
    temp2=circ3[1];
    circ3[0]=temp1*cos(-theta)-temp2*sin(-theta);
    circ3[1]=temp1*sin(-theta)+temp2*cos(-theta);
    circ3[2]=circ[2].r;
    
    // Computation of the goal coordinates.
    double x2=circ2[0];
    double x3=circ3[0];
    double y3=circ3[1];
    double r1=circ1[2];
    double r2=circ2[2];
    double r3=circ3[2];
    
    double x = (r1*r1 - r2*r2 + x2*x2) / (2*x2) ;
    double y = -(r3*r3 - r1*r1 - (x-x3)*(x-x3) - y3*y3) / (2*y3) - (r1*r1 - r2*r2 + x2*x2)*(r1*r1 - r2*r2 + x2*x2) / (8*x2*x2*y3) ;
    
    printf("Coordinate del goal con asse cartesiano rototraslato: %f %f\n",x,y);
    
    // Return to the original coordinates of the simulator system
    temp1=x;
    temp2=y;
    x=temp1*cos(theta)-temp2*sin(theta);
    y=temp1*sin(theta)+temp2*cos(theta);
    
    x=x-offsetX;
    y=y-offsetY;
    
    printf("Coordinate del goal con asse cartesiano originale: %f %f\n",x,y);
    
    // Here we supposed the goal can be placed only in integer coordinates, so we approximate the results of the computation to obtain integer coordinates and remove the small error of the trilateration computation.
    double tempX = x - (int)x;
    double tempY = y - (int)y;
    
    if (tempX > 0 ){
        if (tempX > 0.5){
            x = ceil(x);
        } else {
            x = floor(x);
        }
        
    } else {
        if (tempX < -0.5){
            x = floor(x);
        } else {
            x = ceil(x);
        }
    }
    
    if (tempY > 0 ){
        if (tempY > 0.5){
            y = ceil(y);
        } else {
            y = floor(y);
        }
    } else {
        if (tempY < -0.5){
            y = floor(y);
        } else {
            y = ceil(y);
        }
    }
    
    
    printf("Coordinate del goal con asse cartesiano originale (ARROTONDATE): %f %f\n",x,y);
    goal[0] = x;
    goal[1] = y;
    
    
}


/* Computes the average value coming from a proximity sensor.*/
double avgDist(double *vector,int num){
	int i=0;
	double tot=0;
	for (i=0;i<num;i++){
		tot=tot + vector[i];
	}
	return tot/num;
}

/* Function used to check if the robot is on the rect joining start and goal point, during the boundary following
movement.*/
int isOnR(Pose* pose,double m,double q,int flag){
	
	if(flag == 1){
		if(fabs(pose->x - q) < 0.4 ){
			return 1;
		} else {
			return 0;
		}
	}
	if( fabs((pose->y - q) - (m * pose->x)) < 0.4 ){
		//printf("Is ON rect\n");
		return 1;
	} else{
		return 0;
	}
}


void adjVel(Pose* pose, double* target, double* v, double* w){
    
    double dx = -pose->x + target[0];
    double dy = -pose->y + target[1];
    if (fabs(dy) < 0.005){
        dy = 0;
    }
    double th = atan2(dy,dx) - pose->yaw;
    *v = 1;
    *w = 1 * th;
    
}

/* The calcVel function is used to compute the right angular velocity and turn the robot in the direction of the target.*/
void calcVel(Pose* pose, double* target, double* v, double* w){
    
    double dx = -pose->x + target[0];
    double dy = -pose->y + target[1];
    if (fabs(dy) < 0.005){
        dy = 0;
    }
    double th = atan2(dy,dx) - pose->yaw;
    *v = 0;
    *w = 1 * th;
    
}

void bug2(double *goal,Pose* pose, proxMeas* proxmeas,FILE *f){
	
	double startX = pose->x;
	double startY = pose->y;
	printf("startX: %d, startY: %d\n", startX, startY);
	double m,q;
	int deg = 0;
	int result;
	int flag;
	irMeas irmeas1;
	irMeas irmeas2;
	irMeas irmeas3;
	
	double dx = goal[0]-startX;
	double dy = goal[1]-startY;
	
	if (fabs(dx) < 1){
		deg = 1;
		m = 1;
		q = goal[0];
	} else {
		m = dy/dx;
		q = goal[1] - m*goal[0];
	}
	
	//printf("m := %f  q := %f   deg:= %d\n",m,q,deg);

	/*Until the robot reaches the goal, or the boundary following method gives the impossible result, the two phases of
	the navigation method are repeated. */
	while(1){
		
		printf("Entering motion to goal\n");
		int goalFound = motionToGoal(goal,pose,proxmeas,&irmeas1, &irmeas2, &irmeas3, f);

		if(goalFound == 1){
			setSpeed(f, "robot", "motion", 0,0);
			printf("Goal raggiunto");
			break;
		}
		
		printf("Entering boundary following\n");
		result = boundaryFollowing(goal, pose,proxmeas,&irmeas1,&irmeas2,&irmeas3,f,m,q,deg);
		
		if(result == 1){
			printf("Goal non raggiungibile\n");
			flag = setSpeed(f, "robot", "motion", 0,0);
			if ( flag < 0 ) {
				printf("primo errore\n");
				printf("Errore 2\n");
				fclose(f);
				return;
			}
			break;
		}
		
	}

}

/* Method that implements the motion to goal phase of the navigation.*/
int motionToGoal(double *goal,Pose* pose, proxMeas* proxmeas, irMeas *irmeas1, irMeas *irmeas2, irMeas *irmeas3, FILE *f){
    
    double v = 0;
    double w = 1;
    int flag;
    
    if(pose->y > goal[1]){
        w=-1;
    }
    
    /* First of all, after the leave point has been found, the robot has to turn in the direction of the goal. */
    while (fabs(w) > 0.05){
        getSensorsMeasurements(f,"robot",pose,proxmeas);
        calcVel(pose,goal, &v, &w);
        flag = setSpeed(f, "robot", "motion", v,w);
        if ( flag < 0 ) {
            printf("primo errore\n");
            printf("Errore 2\n");
            fclose(f);
            return 0;
        }
        
        getSensorsMeasurements(f,"robot",pose,proxmeas);
        
    }
    
    
    flag = getIR(f,"robot","IR1",irmeas1);
    if(flag<0){
        printf("Errore 4 \n");
        fclose(f);
        return 4;
    }
    
    flag = getIR(f,"robot","IR2",irmeas2);
    if(flag<0){
        printf("Errore 4 \n");
        fclose(f);
        return 4;
    }
    
    flag = getIR(f,"robot","IR3",irmeas3);
    if(flag<0){
        printf("Errore 4 \n");
        fclose(f);
        return 4;
    }
    /* The "mean" values are the average value coming out of each of the three front proximity sensor. */
	double mean1 = avgDist(irmeas1->dist,irmeas1->numP);
	double mean2 = avgDist(irmeas2->dist,irmeas2->numP);
	double mean3 = avgDist(irmeas3->dist,irmeas3->numP);
	
	/* While the robot has free space in front of it, it goes straight to the goal. */
	while((mean1 >= 1.2) && (mean2 >= 0.7) && (mean3 >= 0.7)){
		printf("MOTION = %lf, %lf, %lf\n",mean1, mean2, mean3);
		adjVel(pose,goal, &v, &w);
		flag = setSpeed(f, "robot", "motion", v,w);
		if ( flag < 0 ) {
			printf("Errore 2\n");
			fclose(f);
			return 4;
		}

		flag = getIR(f,"robot","IR1",irmeas1);
		if(flag<0){
			printf("Errore 4 \n");
			fclose(f);
			return 4;
		}
		
		flag = getIR(f,"robot","IR2",irmeas2);
		if(flag<0){
			printf("Errore 4 \n");
			fclose(f);
			return 4;
		}
		
		flag = getIR(f,"robot","IR3",irmeas3);
		if(flag<0){
			printf("Errore 4 \n");
			fclose(f);
			return 4;
		}
		
		getSensorsMeasurements(f,"robot",pose,proxmeas);
		printf(" sono fuori %f\n", sqrt((goal[0]-pose->x)*(goal[0]-pose->x) + (goal[1]-pose->y)*(goal[1]-pose->y)));
		
		if(sqrt((goal[0]-pose->x)*(goal[0]-pose->x) + (goal[1]-pose->y)*(goal[1]-pose->y)) < 1.2) {
			printf(" sono dentro %f %f\n", fabs(pose->x), fabs(pose->y));
			return 1;

		}
			
		mean1 = avgDist(irmeas1->dist,irmeas1->numP);
		mean2 = avgDist(irmeas2->dist,irmeas2->numP);
		mean3 = avgDist(irmeas3->dist,irmeas3->numP);
	
    }
}

void moveTril(struct circonference *circ,Pose* pose, proxMeas* proxmeas, double* v, double* w, FILE *f){

	double offsetX;
	double offsetY;
	double d=0;
	
 	int i = 0;
	double mean;

	circ[i].x = pose->x;
	circ[i].y = pose->y;
	circ[i].r = proxmeas->objects[0].dist;

	
	while(1){

		offsetX = pose->x;
		offsetY = pose->y;
		irMeas irmeas1;
		irMeas irmeas2;
		int flag = getIR(f,"robot","IR1",&irmeas1);
		if(flag<0){
			printf("Errore 4 \n");
			fclose(f);
			return;
		}

		
		mean = avgDist(irmeas1.dist,irmeas1.numP);
		
		
		if(mean > 1.3){
			
			i++;
			d=0;

			while(d<0.8){
				flag = getIR(f,"robot","IR1",&irmeas1);
				if(flag<0){
					printf("Errore 4 \n");
					fclose(f);
					return;
				}
				if(irmeas1.dist[10] < 0.1){
					break;
					i--;
				}
				d=sqrt((pose->x-offsetX)*(pose->x-offsetX)+(pose->y-offsetY)*(pose->y-offsetY));
				
				flag = setSpeed(f, "robot", "motion", 1,0);
				if ( flag < 0 ) {
					printf("primo errore\n");
					printf("Errore 2\n");
					fclose(f);
					return;
				}	
		
				
				getSensorsMeasurements(f,"robot",pose,proxmeas);
				
			}
			flag = setSpeed(f, "robot", "motion", 0,0);
			if ( flag < 0 ) {
				printf("primo errore\n");
				printf("Errore 2\n");
				fclose(f);
				return;
			}
			
			usleep(100000);
			getSensorsMeasurements(f,"robot",pose,proxmeas);
			circ[i].x = pose->x;
			circ[i].y = pose->y;
			circ[i].r = proxmeas->objects[0].dist;
			
			double dx1 = fabs(circ[2].x - circ[1].x);
			double dy1 = fabs(circ[2].y - circ[1].y);
			double dx2 = fabs(circ[2].x - circ[0].x);
			double dy2 = fabs(circ[2].y - circ[0].y);
			
			
			
			if (i == 2 && ((dx1<0.5 || dy1<0.5) && (dx2<0.5 || dy2<0.5))) {
				i=1;
			}

			if (i==2){
				break;
			}
			
			double initialYaw = pose->yaw;
			double tempW;

			int flag = getIR(f,"robot","IR2",&irmeas2);
			if(flag<0){
				printf("Errore 4 \n");
				fclose(f);
				return;
			}
			if(avgDist(irmeas2.dist,irmeas2.numP)<2){
				tempW=-1;
			} else {
				tempW=1;
			}
			
			while(fabs(pose->yaw - initialYaw) < 1.5){

				flag = setSpeed(f, "robot", "motion", 0,tempW);
				if ( flag < 0 ) {
					printf("primo errore\n");
					printf("Errore 2\n");
					fclose(f);
					return;
				}	
				getSensorsMeasurements(f,"robot",pose,proxmeas);
				usleep(100000);
			}
		
		} else {

			while(mean <= 1.3){
				flag = setSpeed(f, "robot", "motion", 0,1);
				if ( flag < 0 ) {
					printf("primo errore\n");
					printf("Errore 2\n");
					fclose(f);
					return;
				}
				mean = avgDist(irmeas1.dist,irmeas1.numP);
				flag = getIR(f,"robot","IR1",&irmeas1);
				if(flag<0){
					printf("Errore 4 \n");
					fclose(f);
					return;
				}
				
				usleep(100000);
			}
		}
	}
	printf("Circumference1 : %f %f %f\n",circ[0].x,circ[0].y,circ[0].r);
	printf("Circumference2 : %f %f %f\n",circ[1].x,circ[1].y,circ[1].r);
	printf("Circumference3 : %f %f %f\n",circ[2].x,circ[2].y,circ[2].r);
}




/* Method that implements the boundary following mode, in which the robot moves along the border of the obstacle*/
int boundaryFollowing(double *goal, Pose* pose, proxMeas* proxmeas,irMeas *irmeas1,irMeas *irmeas2,irMeas *irmeas3,FILE *f, double m, double q, int deg){
    
    getSensorsMeasurements(f,"robot",pose,proxmeas);
    double hitX = pose->x; // X coordinate of the hit point
    double hitY = pose->y; // Y coordiante of the hit point
    double hitYaw = pose->yaw;
    double deltaYaw=0;
    double distHit = sqrt((goal[0]-hitX)*(goal[0]-hitX) + (goal[1]-hitY)*(goal[1]-hitY)); // Distance between the hit point and the goal
    double tempDist;
    int impossible = 0; // Flag parameter used to see if the goal is not reachable
    
    printf("hitX := %f   hitY := %f\n",hitX,hitY);
    
    int flagR; // Flag used to see when the robot is passing on the rect joining start and goal point
    
    int flag = getIR(f,"robot","IR1",irmeas1);
    if(flag<0){
        printf("Errore 4 \n");
        fclose(f);
        return 3;
    }
    flag = getIR(f,"robot","IR3",irmeas3);
    if(flag<0){
        printf("Errore 4 \n");
        fclose(f);
        return;
    }
    
    double mean = avgDist(irmeas1->dist,irmeas1->numP);
    
    while(irmeas1->dist[0]<1.5 || irmeas1->dist[20]<1.5 || irmeas3->dist[0]<0.9){
        printf(" ruotando %f %f %f\n", irmeas1->dist[0], irmeas1->dist[20], irmeas3->dist[0]);
        deltaYaw = fabs(hitYaw-pose->yaw);
        flag = setSpeed(f, "robot", "motion", 0,1);
        if ( flag < 0 ) {
            printf("primo errore\n");
            printf("Errore 2\n");
            fclose(f);
            return 3;
        }
        mean = avgDist(irmeas1->dist,irmeas1->numP);
        flag = getIR(f,"robot","IR1",irmeas1);
        if(flag<0){
            printf("Errore 4 \n");
            fclose(f);
            return;
        }
        flag = getIR(f,"robot","IR3",irmeas3);
        if(flag<0){
            printf("Errore 4 \n");
            fclose(f);
            return;
        }
        getSensorsMeasurements(f,"robot",pose,proxmeas);
        
    }
    
    while (1){
        
        flag = getIR(f,"robot","IR1",irmeas1);
        if(flag<0){
            printf("Errore 4 \n");
            fclose(f);
            return 3;
        }
        flag = getIR(f,"robot","IR3",irmeas3);
        if(flag<0){
            printf("Errore 4 \n");
            fclose(f);
            return 3;
        }
        
        flag = getIR(f,"robot","IR2",irmeas2);
        if(flag<0){
            printf("Errore 4 \n");
            fclose(f);
            return 3;
        }
        
        getSensorsMeasurements(f,"robot",pose,proxmeas);
        
        flag = setSpeed(f, "robot", "motion", 1,0);
        if ( flag < 0 ) {
            printf("primo errore\n");
            printf("Errore 2\n");
            fclose(f);
            return 3;
        }
        float mean3 = avgDist(irmeas3->dist,irmeas3->numP);
        float mean2 = avgDist(irmeas2->dist,irmeas2->numP);
        float mean1 = avgDist(irmeas1->dist,irmeas1->numP);
        
        /* mean3 is the avg value coming out of the right side proximity sensor. Since the robot turns left at the
		beginning of the boundary following, we check the right sensor to keep it close to the obstacle*/
		//if(mean3 > 0.6 || irmeas3->dist[19] < 0.4){
		if(mean3 > 0.8 || mean3 < 0.6){
			
			float v = 1;
			float k=1;
			float diff = (0.5-mean3);
			float w = (diff * k);

			int locked = 0;
			double tempPosX = pose->x;
			double tempPosY = pose->y;
			time_t t0 = time(0);
			
			/*Locked is a parameter set to 1 when the robot has "free view" from the front sensor but it is locked on left 
			side. When locked is set to 1 the robot turns of almost 90 degrees to find free space.
			
			Another control is made if the robot is stuck in a position for more than 3 seconds. Also in this cae it turns
			to find free space and continue the boundary following.*/

			int primoHit = 1;
			//finchè il robot non è nè troppo vicino, nè troppo lontano dall'ostacolo
			//while(mean3 > 0.6 || irmeas3->dist[19] < 0.4){
			while(mean3 > 0.8 || mean3 < 0.6){
				printf("means primo while : %f %f %f\n", mean1, mean2, mean3);

				while((fabs(fabs(pose->x) - fabs(hitX)) < 0.3 || fabs(fabs(pose->y) - fabs(hitY)) < 0.3) && primoHit == 1) {
					//printf("%f, %f\n",fabs(fabs(pose->x) - fabs(hitX)), fabs(fabs(pose->y) - fabs(hitY)));
					getSensorsMeasurements(f,"robot",pose,proxmeas);
				}
				primoHit = 0;
		/* The following condition is used to manage the case in which the goal is not reachable. The method returns the
				value "impossible == 1" when during the boundary following phase the robot returns on the hit point without 
				finding a leave point.*/
				getSensorsMeasurements(f,"robot",pose,proxmeas);
				flagR = isOnR(pose,m,q,deg);
				if(flagR == 1) {
					printf("%f, %f\n", pose->x, pose->y);
				}
				tempDist = sqrt((goal[0]-pose->x)*(goal[0]-pose->x) + (goal[1]-pose->y)*(goal[1]-pose->y));
				if(flagR == 1)
					printf("TTT tra goal e robot: %f tra hit e gol: %f\n", tempDist, distHit);
				//printf("impossibile %i %f %f\n",flagR, fabs(fabs(pose->x) - fabs(hitX)), fabs(fabs(pose->y) - fabs(hitY)));
				if(flagR == 1 && sqrt((hitX-pose->x)*(hitX-pose->x) + (hitY-pose->y)*(hitY-pose->y)) < 1.5){
			
					impossible = 1;
			
					return impossible;
			
				}

				if(mean3 >= 1.2 && mean2>=0.5 && locked == 0){
					v = 1;
					w = -1;
				}
				
				if(locked == 0 && mean3 < 1.4){
					v = 1;
					diff = (0.7-mean3);
					w = (diff * k);
				} 

				
				
				if(mean2 < 0.2 || locked == 1){
					locked = 1;
					v=0;
					w=1;
				}
				
				flag = getIR(f,"robot","IR1",irmeas1);
				if(flag<0){
					printf("Errore 4 \n");
					fclose(f);
					return 3;
				}	
				flag = getIR(f,"robot","IR3",irmeas3);
				if(flag<0){
					printf("Errore 4 \n");
					fclose(f);
					return 3;
				}	
				flag = getIR(f,"robot","IR2",irmeas2);
				if(flag<0){
					printf("Errore 4 \n");
					fclose(f);
					return 3;
				}	
				flag = setSpeed(f, "robot", "motion", v,w);
				//printf("w e v %f %f\n", w, v);
				if ( flag < 0 ) {
					printf("primo errore\n");
					printf("Errore 2\n");
					fclose(f);
					return 3;
				}
				mean1 = avgDist(irmeas1->dist,irmeas1->numP);
				mean3 = avgDist(irmeas3->dist,irmeas3->numP);
				mean2 = avgDist(irmeas2->dist,irmeas2->numP);
				getSensorsMeasurements(f,"robot",pose,proxmeas);
				flagR = isOnR(pose,m,q,deg);
				if(flagR == 1) {
					printf("%f, %f\n", pose->x, pose->y);
				}
				
				/* The following condition is the one evaluated when a leave point is found, and so the robot has to exit
				the boundary following phase.*/
				tempDist = sqrt((goal[0]-pose->x)*(goal[0]-pose->x) + (goal[1]-pose->y)*(goal[1]-pose->y));
				if(flagR == 1)
					printf("GGGG tra goal e robot: %f tra hit e gol: %f\n", tempDist, distHit);
				if(flagR == 1 && (tempDist < distHit) && (fabs(tempDist - distHit) > 0.2)){
					printf("GGGG %f %f\n", tempDist, distHit);
					return 0;
			
				}
				
				/* The following block of code is used to move the robot in the case it cannot move anymore. It may 
				happen when the obstacle is so thin that it actually blocks the robot without being detected by the
				sensors. The code is executed if the robot is stuck in a position for more than 5 seconds.*/
				time_t t1 = time(0);
				getSensorsMeasurements(f,"robot",pose,proxmeas);
				if((fabs(tempPosX - pose->x) >0.1) && (fabs(tempPosY - pose->y) > 0.1)){
					getSensorsMeasurements(f,"robot",pose,proxmeas);
					t0 = time(0);
					tempPosX = pose->x;
					tempPosY = pose->y;
					if(fabs(t1-t0) > 5){
						printf("HELP BLOCCATO! MUOVERSI\n");
						getSensorsMeasurements(f,"robot",pose,proxmeas);
						double initialYaw = pose->yaw;
						while(fabs(fabs(initialYaw)-fabs(pose->yaw))< 1.3){
							flag = setSpeed(f, "robot", "motion", 0,0.5);
							if ( flag < 0 ) {
								printf("primo errore\n");
								printf("Errore 2\n");
								fclose(f);
								return 3;
							}
							getSensorsMeasurements(f,"robot",pose,proxmeas);
							t0 = time(0);
						
						}
						tempPosX = pose->x;
						tempPosY = pose->y;
						printf("SBLOCCATO\n");
						break;
					
					
					}
				}
				//printf("mean1 break = %f\n", mean1);
				if(mean1 < 1.7){
					
					break;
					
				}
				
			}
		}
		
		mean1 = avgDist(irmeas1->dist,irmeas1->numP);
		//printf("mean1 if = %f\n", mean1);
		if(mean1 < 1.7){
			
			getSensorsMeasurements(f,"robot",pose,proxmeas);
			while(mean1 < 1.7){
				printf("means nel secondo while : %f %f %f\n", mean1, mean2, mean3);
				flag = getIR(f,"robot","IR1",irmeas1);
				if(flag<0){
					printf("Errore 4 \n");
					fclose(f);
					return 3;
				}	
				//printf("ruotando e mean1 = %f\n", mean1);
				flag = setSpeed(f, "robot", "motion", 0,1);
				if ( flag < 0 ) {
					printf("primo errore\n");
					printf("Errore 2\n");
					fclose(f);
					return 3;
				}
				mean1 = avgDist(irmeas1->dist,irmeas1->numP);
				
				getSensorsMeasurements(f,"robot",pose,proxmeas);
				flagR = isOnR(pose,m,q,deg);
				if(flagR == 1) {
					printf("%f, %f\n", pose->x, pose->y);
				}
				/* Exit condition evaluated*/
				if(flagR == 1) printf("FFF tra goal e robot: %f tra hit e gol: %f\n", tempDist, distHit);
				tempDist = sqrt((goal[0]-pose->x)*(goal[0]-pose->x) + (goal[1]-pose->y)*(goal[1]-pose->y));
				if(flagR == 1 && (tempDist < distHit) && (fabs(tempDist - distHit) > 0.2)){
					printf("FFF %f %f\n", tempDist, distHit);
					return 0;
			
				}
			

			}
			
		}
		
		/* The following condition is used to manage the case in which the goal is not reachable. The method returns the
		value "impossible == 1" when during the boundary following phase the robot returns on the hit point without 
		finding a leave point.*/
		getSensorsMeasurements(f,"robot",pose,proxmeas);
		flagR = isOnR(pose,m,q,deg);
		if(flagR == 1) {
			printf("%f, %f\n", pose->x, pose->y);
		}
		//printf("impossibile %i %f %f\n",flagR, fabs(pose->x - hitX), fabs(pose->y - hitY));
		if(flagR == 1 && sqrt((hitX-pose->x)*(hitX-pose->x) + (hitY-pose->y)*(hitY-pose->y)) < 1.5){
	
			impossible = 1;
			
			return impossible;	
		}
		
		/* The following condition has been added to avoid false positive cases, due to the shape of the robot, that may 
		not return exactly on the hit point.*/
		tempDist = sqrt((goal[0]-pose->x)*(goal[0]-pose->x) + (goal[1]-pose->y)*(goal[1]-pose->y));
		if(flagR == 1) printf("HHH tra goal e robot: %f tra hit e gol: %f\n", tempDist, distHit);
		if(flagR == 1 && (tempDist < distHit) && (fabs(tempDist - distHit) > 0.2)){
			
			return 0;
			
		}      
    }
}
    
        
int main() {
            
	double v = 0;
	double w = 0;
        
	
	/* Allocate memory for the vector of circles used for the trilateration. */
	struct circonference *circ;
	int dim = 3;
	if (!(circ = malloc(dim * sizeof(struct circonference)))) {
		perror("Errore allocazione memoria\n");
		return 1;
	}
	Pose pose;
	proxMeas proxmeas;
	
    
    // Set the address and the port for the socket communication
    char address[] = "127.0.0.1";
    int serverPort = 4000;

    // Set the name of the robot
    char parent[] = "robot";

    // Open the communication (here all the components use the same port)
    int sock;
    sock = client_doSock(address, serverPort);
    if ( sock == -1) {
        printf("Errore 1\n");
        return 1;
    }
    FILE * f = fdopen(sock, "r+");
    
    getSensorsMeasurements(f,parent,&pose,&proxmeas);
    
    double offsetX = pose.x;
    double offsetY = pose.y;

	//printf("offsetX: %d, offsetY: %d\n", offsetX, offsetY);
   
    getSensorsMeasurements(f,parent,&pose,&proxmeas);

	irMeas irmeas1;
	irMeas irmeas2;
	irMeas irmeas3;
	
	int flag = getIR(f,parent,"IR1",&irmeas1);
	if(flag<0){
		printf("Errore 4 \n");
		fclose(f);
		return 3;
	}
	
	/* The movement of the robot is divided in three consecutive calls: first moveTril to save the three reference 
	points. Than the goal point is computed with the trilateration. finally the bug2 algorithm is executed to reach the
	goal. */
	double goal[2];
	
    moveTril(circ,&pose,&proxmeas,&v,&w,f);
	
    trilateration(circ,-offsetX,-offsetY,goal);
	
    bug2(goal,&pose,&proxmeas,f);
    
    fclose(f);

    return 0;
}

